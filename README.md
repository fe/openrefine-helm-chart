# Openrefine Custom Container and Helm Chart

As long as this is not better engineered, you basically have to do the following to upgrade

```
# current version
docker run -i --rm mikefarah/yq@sha256:eed5d07d2f6a6bd882be5519e98ca2cd3dc3819005820d80b83ae6f3d774e639 '.image.tag'  < openrefine/values.yaml

openrefineversion="3.7.9"
docker build . --build-arg openrefineversion=${openrefineversion}
docker build . -t harbor.gwdg.de/sub-fe-pub/openrefine:${openrefineversion}
docker push harbor.gwdg.de/sub-fe-pub/openrefine:${openrefineversion}
```
