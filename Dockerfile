FROM docker.io/openjdk:17-slim-bullseye
ARG openrefineversion=3.7.9
RUN apt update && apt install -yyy procps curl && apt upgrade -yyy
ADD https://github.com/OpenRefine/OpenRefine/releases/download/$openrefineversion/openrefine-linux-$openrefineversion.tar.gz /tmp/openrefine.tar.gz

WORKDIR /openrefine
RUN tar -xz --strip 1 -f /tmp/openrefine.tar.gz

VOLUME /openrefine-data

EXPOSE 3333

ENTRYPOINT ["/openrefine/refine"]
CMD ["-i", "0.0.0.0", "-d", "/openrefine-data"]